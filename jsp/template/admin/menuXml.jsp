<%@ page contentType="text/xml; charset=utf-8"%>
<?xml version="1.0" encoding="utf-8"?>
<tree>
<!-- Finder - Powered by Finder -->
    <treeNode title="主机管理" href="?action=admin.host.list"/>
    <%
        if(Boolean.TRUE.equals(request.getAttribute("userSupport"))) {
    %>
    <treeNode title="用户管理" href="?action=admin.user.query" expand="true">
        <treeNode title="全部用户" href="?action=admin.user.list"/>
        <treeNode title="添加用户" href="?action=admin.user.edit"/>
        <treeNode title="修改用户" href="?action=admin.user.query"/>
        <treeNode title="删除用户" href="?action=admin.user.query"/>
    </treeNode>
    <%
        }
    %>
    <treeNode title="权限管理" href="?action=admin.user.host.policy" expand="true">
        <treeNode title="主机权限" href="?action=admin.user.host.policy"/>
        <treeNode title="文件权限" href="?action=admin.user.file.policy"/>
    </treeNode>
    <treeNode title="系统设置" href="?action=admin.config.list" expand="true">
        <treeNode title="一般设置" href="?action=admin.config.edit"/>
        <treeNode title="安全设置" href="?action=admin.security.config.edit"/>
    </treeNode>
    <treeNode title="系统更新" href="?action=admin.upgrade.index"/>
    <treeNode title="系统信息" href="?action=admin.system.info"/>
</tree>
