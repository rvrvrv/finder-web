/*
 * $RCSfile: ConfigEditTemplate.java,v $
 * $Revision: 1.1 $
 *
 * JSP generated by JspCompiler-1.0.0
 */
package com.skin.finder.admin.servlet.template;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.skin.finder.config.ConfigFactory;


/**
 * <p>Title: ConfigEditTemplate</p>
 * <p>Description: </p>
 * @author JspKit
 * @version 1.0
 */
public class ConfigEditTemplate extends com.skin.finder.web.servlet.JspServlet {
    private static final long serialVersionUID = 1L;
    private static final ConfigEditTemplate instance = new ConfigEditTemplate();


    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    public static void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        instance.service(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=utf-8");
        OutputStream out = response.getOutputStream();

        out.write(_jsp_string_2, 0, _jsp_string_2.length);
        out.write(_jsp_string_3, 0, _jsp_string_3.length);
        out.write(_jsp_string_4, 0, _jsp_string_4.length);
        out.write(_jsp_string_5, 0, _jsp_string_5.length);
        out.write(_jsp_string_6, 0, _jsp_string_6.length);
        out.write(_jsp_string_7, 0, _jsp_string_7.length);
        out.write(_jsp_string_8, 0, _jsp_string_8.length);
        out.write(_jsp_string_9, 0, _jsp_string_9.length);
        out.write(_jsp_string_10, 0, _jsp_string_10.length);
        out.write(_jsp_string_11, 0, _jsp_string_11.length);
        out.write(_jsp_string_12, 0, _jsp_string_12.length);
        out.write(_jsp_string_13, 0, _jsp_string_13.length);
        out.write(_jsp_string_14, 0, _jsp_string_14.length);
        out.write(_jsp_string_15, 0, _jsp_string_15.length);
        out.write(_jsp_string_16, 0, _jsp_string_16.length);
        out.write(_jsp_string_17, 0, _jsp_string_17.length);
        out.write(_jsp_string_18, 0, _jsp_string_18.length);
        out.write(_jsp_string_19, 0, _jsp_string_19.length);
        out.write(_jsp_string_20, 0, _jsp_string_20.length);
        out.write(_jsp_string_21, 0, _jsp_string_21.length);
        out.write(_jsp_string_22, 0, _jsp_string_22.length);
        out.write(_jsp_string_23, 0, _jsp_string_23.length);
        out.write(_jsp_string_24, 0, _jsp_string_24.length);
        this.write(out, request.getAttribute("contextPath"));
        out.write(_jsp_string_26, 0, _jsp_string_26.length);
        out.write(_jsp_string_27, 0, _jsp_string_27.length);
        out.write(_jsp_string_28, 0, _jsp_string_28.length);
        out.write(_jsp_string_29, 0, _jsp_string_29.length);
        out.write(_jsp_string_30, 0, _jsp_string_30.length);
        this.write(out, (ConfigFactory.getSessionTimeout()));
        out.write(_jsp_string_32, 0, _jsp_string_32.length);
        out.write(_jsp_string_33, 0, _jsp_string_33.length);
        out.write(_jsp_string_34, 0, _jsp_string_34.length);
        this.write(out, (ConfigFactory.getNavBar()));
        out.write(_jsp_string_36, 0, _jsp_string_36.length);
        out.write(_jsp_string_37, 0, _jsp_string_37.length);
        out.write(_jsp_string_38, 0, _jsp_string_38.length);
        out.write(_jsp_string_39, 0, _jsp_string_39.length);
        this.write(out, (ConfigFactory.getTextType()));
        out.write(_jsp_string_41, 0, _jsp_string_41.length);
        out.write(_jsp_string_42, 0, _jsp_string_42.length);
        out.write(_jsp_string_43, 0, _jsp_string_43.length);
        this.write(out, (ConfigFactory.getOperateButton()));
        out.write(_jsp_string_45, 0, _jsp_string_45.length);
        this.write(out, (ConfigFactory.getOperateButton()));
        out.write(_jsp_string_47, 0, _jsp_string_47.length);
        this.write(out, (ConfigFactory.getOperateButton()));
        out.write(_jsp_string_49, 0, _jsp_string_49.length);
        this.write(out, (ConfigFactory.getOperateButton()));
        out.write(_jsp_string_51, 0, _jsp_string_51.length);
        this.write(out, (ConfigFactory.getOperateButton()));
        out.write(_jsp_string_53, 0, _jsp_string_53.length);
        this.write(out, (ConfigFactory.getOperateButton()));
        out.write(_jsp_string_55, 0, _jsp_string_55.length);
        out.write(_jsp_string_56, 0, _jsp_string_56.length);
        out.write(_jsp_string_57, 0, _jsp_string_57.length);
        out.write(_jsp_string_58, 0, _jsp_string_58.length);
        this.write(out, (ConfigFactory.getUploadPartSize()));
        out.write(_jsp_string_60, 0, _jsp_string_60.length);
        out.write(_jsp_string_61, 0, _jsp_string_61.length);
        out.write(_jsp_string_62, 0, _jsp_string_62.length);
        this.write(out, (ConfigFactory.getDemoUserName()));
        out.write(_jsp_string_64, 0, _jsp_string_64.length);
        out.write(_jsp_string_65, 0, _jsp_string_65.length);
        out.write(_jsp_string_66, 0, _jsp_string_66.length);
        this.write(out, (ConfigFactory.getDemoPassword()));
        out.write(_jsp_string_68, 0, _jsp_string_68.length);
        out.write(_jsp_string_69, 0, _jsp_string_69.length);
        out.write(_jsp_string_70, 0, _jsp_string_70.length);
        this.write(out, (ConfigFactory.getUpdateCheck()));
        out.write(_jsp_string_72, 0, _jsp_string_72.length);
        out.write(_jsp_string_73, 0, _jsp_string_73.length);
        out.write(_jsp_string_74, 0, _jsp_string_74.length);
        out.write(_jsp_string_75, 0, _jsp_string_75.length);
        out.write(_jsp_string_76, 0, _jsp_string_76.length);

        out.flush();
    }

    protected static final byte[] _jsp_string_2 = b("<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\r\n");
    protected static final byte[] _jsp_string_3 = b("<meta http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n<meta http-equiv=\"Cache-Control\" content=\"no-cache\"/>\r\n");
    protected static final byte[] _jsp_string_4 = b("<meta http-equiv=\"Expires\" content=\"0\"/>\r\n<title>Finder - Powered by Finder</title>\r\n");
    protected static final byte[] _jsp_string_5 = b("<link rel=\"shortcut icon\" href=\"?action=res&path=/finder/images/favicon.png\"/>\r\n");
    protected static final byte[] _jsp_string_6 = b("<link rel=\"stylesheet\" type=\"text/css\" href=\"?action=res&path=/admin/css/form.css\"/>\r\n");
    protected static final byte[] _jsp_string_7 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/jquery-1.7.2.min.js\"></script>\r\n");
    protected static final byte[] _jsp_string_8 = b("<script type=\"text/javascript\" src=\"?action=res&path=/admin/base.js\"></script>\r\n");
    protected static final byte[] _jsp_string_9 = b("<script type=\"text/javascript\">\r\n<!--\r\njQuery(function() {\r\n    var getOperateButton = function() {\r\n");
    protected static final byte[] _jsp_string_10 = b("        var b = [];\r\n\r\n        jQuery(\"input[name=operateButton]:checked\").each(function() {\r\n");
    protected static final byte[] _jsp_string_11 = b("            b[b.length] = this.value;\r\n        });\r\n        return b.join(\", \");\r\n");
    protected static final byte[] _jsp_string_12 = b("    };\r\n\r\n    jQuery(\"#submit\").click(function() {\r\n        var params = {};\r\n        params.sessionTimeout = jQuery.trim(jQuery(\"input[name=sessionTimeout]\").val());\r\n");
    protected static final byte[] _jsp_string_13 = b("        params.topNavBar = jQuery(\"input[name=topNavBar]\").prop(\"checked\");\r\n        params.operateButton = getOperateButton();\r\n");
    protected static final byte[] _jsp_string_14 = b("        params.textType = jQuery.trim(jQuery(\"input[name=textType]\").val());\r\n        params.uploadPartSize = jQuery.trim(jQuery(\"input[name=uploadPartSize]\").val());\r\n");
    protected static final byte[] _jsp_string_15 = b("        params.demoUserName = jQuery.trim(jQuery(\"input[name=demoUserName]\").val());\r\n");
    protected static final byte[] _jsp_string_16 = b("        params.demoPassword = jQuery.trim(jQuery(\"input[name=demoPassword]\").val());\r\n");
    protected static final byte[] _jsp_string_17 = b("        params.updateCheck = jQuery(\"input[name=updateCheck]\").prop(\"checked\");\r\n");
    protected static final byte[] _jsp_string_18 = b("\r\n        jQuery.ajax({\r\n            \"type\": \"get\",\r\n            \"url\": \"?action=admin.config.save\",\r\n");
    protected static final byte[] _jsp_string_19 = b("            \"dataType\": \"json\",\r\n            \"data\": jQuery.param(params, true),\r\n");
    protected static final byte[] _jsp_string_20 = b("            \"error\": function() {\r\n                alert(\"系统错误，请稍后再试！\");\r\n            },\r\n");
    protected static final byte[] _jsp_string_21 = b("            \"success\": function(response) {\r\n                if(response.status != 200) {\r\n");
    protected static final byte[] _jsp_string_22 = b("                    alert(response.message);\r\n                }\r\n                else {\r\n");
    protected static final byte[] _jsp_string_23 = b("                    alert(\"保存并同步成功！\");\r\n                }\r\n                window.location.reload();\r\n");
    protected static final byte[] _jsp_string_24 = b("            }\r\n        });\r\n    });\r\n});\r\n//-->\r\n</script>\r\n</head>\r\n<body contextPath=\"");
    protected static final byte[] _jsp_string_26 = b("\">\r\n<div class=\"menu-bar\">\r\n    <a class=\"button\" href=\"javascript:void(0)\" onclick=\"window.history.back();\"><span class=\"back\"></span>返回</a>\r\n");
    protected static final byte[] _jsp_string_27 = b("    <a class=\"button\" href=\"javascript:void(0)\" onclick=\"window.location.reload();\"><span class=\"refresh\"></span>刷新</a>\r\n");
    protected static final byte[] _jsp_string_28 = b("    <span class=\"seperator\"></span>\r\n</div>\r\n<div class=\"form\">\r\n    <div class=\"title\"><h4>系统设置 - 一般设置</h4></div>\r\n");
    protected static final byte[] _jsp_string_29 = b("    <div class=\"form-row\">\r\n        <div class=\"form-label\">会话有效期：</div>\r\n        <div class=\"form-c400\">\r\n");
    protected static final byte[] _jsp_string_30 = b("            <div class=\"form-field\">\r\n                <input name=\"sessionTimeout\" type=\"text\" class=\"text w300\" placeholder=\"0s\" value=\"");
    protected static final byte[] _jsp_string_32 = b("\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n            <div class=\"form-comment\">用户登录的有效期。可选单位: [d: 天, h: 时, m: 分, s: 秒]；单位不区分大小写, 无单位或者其他都按照秒处理。支持小数形式。</div>\r\n");
    protected static final byte[] _jsp_string_33 = b("        </div>\r\n    </div>\r\n    <div class=\"form-row\">\r\n        <div class=\"form-label\">显示顶部导航：</div>\r\n");
    protected static final byte[] _jsp_string_34 = b("        <div class=\"form-c400\">\r\n            <div class=\"form-field\">\r\n                <input name=\"topNavBar\" type=\"checkbox\" checked-value=\"");
    protected static final byte[] _jsp_string_36 = b("\" value=\"true\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n");
    protected static final byte[] _jsp_string_37 = b("            <div class=\"form-comment\">是否显示顶部导航条。</div>\r\n        </div>\r\n    </div>\r\n");
    protected static final byte[] _jsp_string_38 = b("    <div class=\"form-row\">\r\n        <div class=\"form-label\">允许打开的文本类型：</div>\r\n        <div class=\"form-c400\">\r\n");
    protected static final byte[] _jsp_string_39 = b("            <div class=\"form-field\">\r\n                <input name=\"textType\" type=\"text\" class=\"text w300\" placeholder=\"5M\" value=\"");
    protected static final byte[] _jsp_string_41 = b("\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n            <div class=\"form-comment\">允许使用tail，less，grep打开的文本文件类型，逗号分隔。\"*\"表示全部。</div>\r\n");
    protected static final byte[] _jsp_string_42 = b("        </div>\r\n    </div>\r\n    <div class=\"form-row\">\r\n        <div class=\"form-label\">显示操作按钮：</div>\r\n");
    protected static final byte[] _jsp_string_43 = b("        <div class=\"form-c400\">\r\n            <div class=\"form-field\">\r\n                <p><input name=\"operateButton\" type=\"checkbox\" checked-value=\"");
    protected static final byte[] _jsp_string_45 = b("\" value=\"tail\"/> tail</p>\r\n                <p><input name=\"operateButton\" type=\"checkbox\" checked-value=\"");
    protected static final byte[] _jsp_string_47 = b("\" value=\"less\"/> less</p>\r\n                <p><input name=\"operateButton\" type=\"checkbox\" checked-value=\"");
    protected static final byte[] _jsp_string_49 = b("\" value=\"grep\"/> grep</p>\r\n                <p><input name=\"operateButton\" type=\"checkbox\" checked-value=\"");
    protected static final byte[] _jsp_string_51 = b("\" value=\"open\"/> open</p>\r\n                <p><input name=\"operateButton\" type=\"checkbox\" checked-value=\"");
    protected static final byte[] _jsp_string_53 = b("\" value=\"download\"/> download</p>\r\n                <p><input name=\"operateButton\" type=\"checkbox\" checked-value=\"");
    protected static final byte[] _jsp_string_55 = b("\" value=\"delete\"/> delete</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n");
    protected static final byte[] _jsp_string_56 = b("            <div class=\"form-comment\">显示的操作按钮, 文件列表项后面的操作按钮。</div>\r\n        </div>\r\n");
    protected static final byte[] _jsp_string_57 = b("    </div>\r\n    <div class=\"form-row\">\r\n        <div class=\"form-label\">文件上传分片大小：</div>\r\n");
    protected static final byte[] _jsp_string_58 = b("        <div class=\"form-c400\">\r\n            <div class=\"form-field\">\r\n                <input name=\"uploadPartSize\" type=\"text\" class=\"text w300\" placeholder=\"5M\" value=\"");
    protected static final byte[] _jsp_string_60 = b("\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n            <div class=\"form-comment\">可选单位: [b: 字节, k: 千字节, m: 兆字节, g: 千兆字节]，单位不区分大小写, 无单位或者其他都按照字节处理。支持小数形式。</div>\r\n");
    protected static final byte[] _jsp_string_61 = b("        </div>\r\n    </div>\r\n    <div class=\"form-row\">\r\n        <div class=\"form-label\">演示账号：</div>\r\n");
    protected static final byte[] _jsp_string_62 = b("        <div class=\"form-c400\">\r\n            <div class=\"form-field\">\r\n                <input name=\"demoUserName\" type=\"text\" class=\"text w300\" placeholder=\"User Name\" value=\"");
    protected static final byte[] _jsp_string_64 = b("\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n            <div class=\"form-comment\">显示在登录页的用户名密码。一般用于演示系统或者公共用户。置空则不显示账号密码。</div>\r\n");
    protected static final byte[] _jsp_string_65 = b("        </div>\r\n    </div>\r\n    <div class=\"form-row\">\r\n        <div class=\"form-label\">演示账号密码：</div>\r\n");
    protected static final byte[] _jsp_string_66 = b("        <div class=\"form-c400\">\r\n            <div class=\"form-field\">\r\n                <input name=\"demoPassword\" type=\"text\" class=\"text w300\" placeholder=\"Password\" value=\"");
    protected static final byte[] _jsp_string_68 = b("\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n            <div class=\"form-comment\">显示在登录页的用户名密码。一般用于演示系统或者公共用户。置空则不显示账号密码。</div>\r\n");
    protected static final byte[] _jsp_string_69 = b("        </div>\r\n    </div>\r\n    <div class=\"form-row\">\r\n        <div class=\"form-label\">自动更新检查：</div>\r\n");
    protected static final byte[] _jsp_string_70 = b("        <div class=\"form-c400\">\r\n            <div class=\"form-field\">\r\n                <input name=\"updateCheck\" type=\"checkbox\" checked-value=\"");
    protected static final byte[] _jsp_string_72 = b("\" value=\"true\"/>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n");
    protected static final byte[] _jsp_string_73 = b("            <div class=\"form-comment\">是否开启自动更新检查。</div>\r\n        </div>\r\n    </div>\r\n");
    protected static final byte[] _jsp_string_74 = b("    <div class=\"button\">\r\n        <button id=\"submit\" class=\"button ensure\">保存并同步到集群</button>\r\n");
    protected static final byte[] _jsp_string_75 = b("    </div>\r\n</div>\r\n<div id=\"pageContext\" style=\"display: none;\"></div>\r\n</body>\r\n");
    protected static final byte[] _jsp_string_76 = b("</html>\r\n");

}
