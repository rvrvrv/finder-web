/*
 * $RCSfile: UserHostEditTemplate.java,v $
 * $Revision: 1.1 $
 *
 * JSP generated by JspCompiler-1.0.0
 */
package com.skin.finder.admin.servlet.template;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * <p>Title: UserHostEditTemplate</p>
 * <p>Description: </p>
 * @author JspKit
 * @version 1.0
 */
public class UserHostEditTemplate extends com.skin.finder.web.servlet.JspServlet {
    private static final long serialVersionUID = 1L;
    private static final UserHostEditTemplate instance = new UserHostEditTemplate();


    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    public static void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        instance.service(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=utf-8");
        OutputStream out = response.getOutputStream();

        out.write(_jsp_string_1, 0, _jsp_string_1.length);
        out.write(_jsp_string_2, 0, _jsp_string_2.length);
        out.write(_jsp_string_3, 0, _jsp_string_3.length);
        out.write(_jsp_string_4, 0, _jsp_string_4.length);
        out.write(_jsp_string_5, 0, _jsp_string_5.length);
        out.write(_jsp_string_6, 0, _jsp_string_6.length);
        out.write(_jsp_string_7, 0, _jsp_string_7.length);
        out.write(_jsp_string_8, 0, _jsp_string_8.length);
        out.write(_jsp_string_9, 0, _jsp_string_9.length);
        out.write(_jsp_string_10, 0, _jsp_string_10.length);
        out.write(_jsp_string_11, 0, _jsp_string_11.length);
        out.write(_jsp_string_12, 0, _jsp_string_12.length);
        out.write(_jsp_string_13, 0, _jsp_string_13.length);
        out.write(_jsp_string_14, 0, _jsp_string_14.length);
        out.write(_jsp_string_15, 0, _jsp_string_15.length);
        out.write(_jsp_string_16, 0, _jsp_string_16.length);
        out.write(_jsp_string_17, 0, _jsp_string_17.length);
        out.write(_jsp_string_18, 0, _jsp_string_18.length);
        out.write(_jsp_string_19, 0, _jsp_string_19.length);
        out.write(_jsp_string_20, 0, _jsp_string_20.length);
        out.write(_jsp_string_21, 0, _jsp_string_21.length);
        out.write(_jsp_string_22, 0, _jsp_string_22.length);

    String[] hosts = (String[])(request.getAttribute("hosts"));

    if(hosts == null) {
        hosts = new String[0];
    }

        out.write(_jsp_string_24, 0, _jsp_string_24.length);
        out.write(_jsp_string_25, 0, _jsp_string_25.length);
        out.write(_jsp_string_26, 0, _jsp_string_26.length);
        out.write(_jsp_string_27, 0, _jsp_string_27.length);
        out.write(_jsp_string_28, 0, _jsp_string_28.length);
        out.write(_jsp_string_29, 0, _jsp_string_29.length);

                    for(String hostName : hosts) {
                
        out.write(_jsp_string_31, 0, _jsp_string_31.length);
        this.write(out, (hostName));
        out.write(_jsp_string_33, 0, _jsp_string_33.length);
        this.write(out, (hostName));
        out.write(_jsp_string_35, 0, _jsp_string_35.length);

                    }
                
        out.write(_jsp_string_37, 0, _jsp_string_37.length);
        out.write(_jsp_string_38, 0, _jsp_string_38.length);
        out.write(_jsp_string_39, 0, _jsp_string_39.length);
        out.write(_jsp_string_40, 0, _jsp_string_40.length);
        this.write(out, request.getAttribute("contextPath"));
        out.write(_jsp_string_42, 0, _jsp_string_42.length);

        out.flush();
    }

    protected static final byte[] _jsp_string_1 = b("<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\r\n");
    protected static final byte[] _jsp_string_2 = b("<meta http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n<meta http-equiv=\"Cache-Control\" content=\"no-cache\"/>\r\n");
    protected static final byte[] _jsp_string_3 = b("<meta http-equiv=\"Expires\" content=\"0\"/>\r\n<title>Finder - Powered by Finder</title>\r\n");
    protected static final byte[] _jsp_string_4 = b("<link rel=\"shortcut icon\" href=\"?action=res&path=/finder/images/favicon.png\"/>\r\n");
    protected static final byte[] _jsp_string_5 = b("<link rel=\"stylesheet\" type=\"text/css\" href=\"?action=res&path=/admin/css/form.css\"/>\r\n");
    protected static final byte[] _jsp_string_6 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/jquery-1.7.2.min.js\"></script>\r\n");
    protected static final byte[] _jsp_string_7 = b("<script type=\"text/javascript\" src=\"?action=res&path=/admin/base.js\"></script>\r\n");
    protected static final byte[] _jsp_string_8 = b("<script type=\"text/javascript\">\r\n<!--\r\njQuery(function() {\r\n    jQuery(\"button[name=submit]\").click(function() {\r\n");
    protected static final byte[] _jsp_string_9 = b("        var userName = jQuery.trim(jQuery(\"input[name=userName]\").val());\r\n        var hostName = jQuery.trim(jQuery(\"select[name=hostName]\").val());\r\n");
    protected static final byte[] _jsp_string_10 = b("\r\n        if(userName.length < 1) {\r\n            alert(\"请输入用户名！\");\r\n            return;\r\n");
    protected static final byte[] _jsp_string_11 = b("        }\r\n\r\n        if(hostName.length < 1) {\r\n            alert(\"请选择主机！\");\r\n            return;\r\n");
    protected static final byte[] _jsp_string_12 = b("        }\r\n\r\n        var params = [];\r\n        params[params.length] = \"userName=\" + encodeURIComponent(userName);\r\n");
    protected static final byte[] _jsp_string_13 = b("        params[params.length] = \"hostName=\" + encodeURIComponent(hostName);\r\n\r\n        jQuery.ajax({\r\n");
    protected static final byte[] _jsp_string_14 = b("            \"type\": \"get\",\r\n            \"url\": \"?action=admin.policy.host.add&host=\" + encodeURIComponent(hostName),\r\n");
    protected static final byte[] _jsp_string_15 = b("            \"dataType\": \"json\",\r\n            \"data\": params.join(\"&\"),\r\n            \"error\": function() {\r\n");
    protected static final byte[] _jsp_string_16 = b("                alert(\"系统错误，请稍后再试！\");\r\n            },\r\n            \"success\": function(response) {\r\n");
    protected static final byte[] _jsp_string_17 = b("                if(response.status != 200) {\r\n                    alert(response.message);\r\n");
    protected static final byte[] _jsp_string_18 = b("                    return;\r\n                }\r\n                alert(\"添加成功，您可以继续添加其他主机或者返回到上一个页面！\");\r\n");
    protected static final byte[] _jsp_string_19 = b("            }\r\n        });\r\n    });\r\n});\r\n//-->\r\n</script>\r\n</head>\r\n<body>\r\n<div class=\"menu-bar\">\r\n");
    protected static final byte[] _jsp_string_20 = b("    <a class=\"button\" href=\"javascript:void(0)\" onclick=\"window.history.back();\"><span class=\"back\"></span>返回</a>\r\n");
    protected static final byte[] _jsp_string_21 = b("    <a class=\"button\" href=\"javascript:void(0)\" onclick=\"window.location.reload();\"><span class=\"refresh\"></span>刷新</a>\r\n");
    protected static final byte[] _jsp_string_22 = b("    <span class=\"seperator\"></span>\r\n</div>\r\n");
    protected static final byte[] _jsp_string_24 = b("<div class=\"form\">\r\n    <div class=\"form-row\">\r\n        <div class=\"form-label\">User Name：</div>\r\n");
    protected static final byte[] _jsp_string_25 = b("        <div class=\"form-c300\">\r\n            <div class=\"form-field\">\r\n                <input name=\"userName\" type=\"text\" class=\"text w200\" placeholder=\"User Name\" value=\"\"/>\r\n");
    protected static final byte[] _jsp_string_26 = b("            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n            <div class=\"form-comment\">User Name .</div>\r\n");
    protected static final byte[] _jsp_string_27 = b("        </div>\r\n    </div>\r\n    <div class=\"form-row\">\r\n        <div class=\"form-label\">Host Name</div>\r\n");
    protected static final byte[] _jsp_string_28 = b("        <div class=\"form-c300\">\r\n            <div class=\"form-field\">\r\n                <select name=\"hostName\" type=\"text\" class=\"select w200\" title=\"主机\">\r\n");
    protected static final byte[] _jsp_string_29 = b("                    <option value=\"\">请选择主机</option>\r\n                ");
    protected static final byte[] _jsp_string_31 = b("                    <option value=\"");
    protected static final byte[] _jsp_string_33 = b("\">");
    protected static final byte[] _jsp_string_35 = b("</option>\r\n                ");
    protected static final byte[] _jsp_string_37 = b("                </select>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-m300\">\r\n");
    protected static final byte[] _jsp_string_38 = b("            <div class=\"form-comment\">Host Name .</div>\r\n        </div>\r\n    </div>\r\n");
    protected static final byte[] _jsp_string_39 = b("\r\n    <div class=\"button\">\r\n        <button name=\"submit\" class=\"button ensure\">保 存</button>\r\n");
    protected static final byte[] _jsp_string_40 = b("    </div>\r\n</div>\r\n<div id=\"pageContext\" style=\"display: none;\" contextPath=\"");
    protected static final byte[] _jsp_string_42 = b("\"></div>\r\n</body>\r\n</html>\r\n");

}
